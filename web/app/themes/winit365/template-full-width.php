<?php
/*
Template name
*/
?>

<?php get_header(); ?>
			
	<div id="content" class="row">
	
	    <main id="main" class="large-12 medium-12 columns" role="main">
			
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<?php get_template_part( 'templates/loop', 'page' ); ?>
				
			<?php endwhile; endif; ?>							

		</main> <!-- end #main -->
		
	</div> <!-- end #content -->

<?php get_footer(); ?>
