export class AutoInit {

	constructor( modules ) {
		this.modules = modules
		this.initialize()
	}

	initialize() {
		this.modules.forEach((module) => {
			(new module()).init();
		})
	}

}