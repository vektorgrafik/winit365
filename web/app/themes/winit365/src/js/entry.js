import { AutoInit } from './helpers/VgHelpers'

/*
 |--------------------------------------------------------------------------
 | Foundation Configuration
 |--------------------------------------------------------------------------
 |
 | Edit the following file to include Foundation Javascript Modules.
 | If the project doesn't need to utilize any Foundation Modules,
 | simply comment out the line below.
 |
 */
import './vendor/foundation.js'


/*
 |--------------------------------------------------------------------------
 | Components
 |--------------------------------------------------------------------------
 |
 | Edit the following file to include Foundation Javascript Modules.
 | If the project doesn't need to utilize any Foundation Modules,
 | simply comment out the line below.
 |
 */
import Header from './components/header'


/*
 |--------------------------------------------------------------------------
 | Auto-initialize
 |--------------------------------------------------------------------------
 |
 | This little helper runs the "init" method on each class it is
 | given. So for every component you want initialized immediately,
 | just add it here, and voilá.
 |
 */
 const autoinit = new AutoInit([
 	Header
 ])