function addTemplates(idx) {

	for( var i = 0; i < vektor_acf_templates[idx].layouts.length; i++ ) {
		acf.fields.flexible_content.add( vektor_acf_templates[idx].layouts[i] );
	}

}

function registerTemplateButtons($) {

	$target = $('#post-body-content');
	$wrapper = $('<div></div>')
	$wrapper
		.css({
			padding: '15px 0'
		})
		.insertAfter($target);

	for( var i = 0; i < vektor_acf_templates.length; i++ ) {

		var title = vektor_acf_templates[i].title;

		$button = $('<button></button>');
		$button
			.addClass('button button-primary')
			.attr('data-index', i)
			.css({
				'margin-right': '5px'
			})
			.html(title)
			.appendTo($wrapper)
			.on('click', function(e) {
				e.preventDefault();

				addTemplates( $(this).data('index') );

			});
	}

};
jQuery(document).ready(function() {
	registerTemplateButtons(jQuery)
});