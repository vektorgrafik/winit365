import $ from 'jquery'
import { Foundation } from 'foundation-sites/js/foundation.core'

// Abide
/*import { Abide } from 'foundation-sites/js/foundation.abide'
Foundation.plugin(Abide, 'Abide')*/

// Accordion
/*import { Accordion } from 'foundation-sites/js/foundation.accordion'
Foundation.plugin(Accordion, 'Accordion')*/

// AccordionMenu
/*import { AccordionMenu } from 'foundation-sites/js/foundation.accordionMenu'
Foundation.plugin(AccordionMenu, 'AccordionMenu')*/

// Drilldown
/*import { Drilldown } from 'foundation-sites/js/foundation.drilldown'
Foundation.plugin(Drilldown, 'Drilldown')*/

// Dropdown
/*import { Dropdown } from 'foundation-sites/js/foundation.dropdown'
Foundation.plugin(Dropdown, 'Dropdown')*/

// DropdownMenu
/*import { DropdownMenu } from 'foundation-sites/js/foundation.dropdownMenu'
Foundation.plugin(DropdownMenu, 'DropdownMenu')*/

// Equalizer
/*import { Equalizer } from 'foundation-sites/js/foundation.equalizer'
Foundation.plugin(Equalizer, 'Equalizer')*/

// Interchange
/*import { Interchange } from 'foundation-sites/js/foundation.interchange'
Foundation.plugin(Interchange, 'Interchange')*/

// Magellan
/*import { Magellan } from 'foundation-sites/js/foundation.magellan'
Foundation.plugin(Magellan, 'Magellan')*/

// OffCanvas
import { OffCanvas } from 'foundation-sites/js/foundation.offcanvas'
Foundation.plugin(OffCanvas, 'OffCanvas')

// Orbit
/*import { Orbit } from 'foundation-sites/js/foundation.orbit'
Foundation.plugin(Orbit, 'Orbit')*/

// Reveal
/*import { Reveal } from 'foundation-sites/js/foundation.reveal'
Foundation.plugin(Reveal, 'Reveal')*/

// Slider
/*import { Slider } from 'foundation-sites/js/foundation.slider'
Foundation.plugin(Slider, 'Slider')*/

// SmoothScroll
/*import { SmoothScroll } from 'foundation-sites/js/foundation.smoothScroll'
Foundation.plugin(SmoothScroll, 'SmoothScroll')*/

// Sticky
/*import { Sticky } from 'foundation-sites/js/foundation.sticky'
Foundation.plugin(Sticky, 'Sticky')*/

// Tabs
/*import { Tabs } from 'foundation-sites/js/foundation.tabs'
Foundation.plugin(Tabs, 'Tabs')*/

// Toggler
/*import { Toggler } from 'foundation-sites/js/foundation.toggler'
Foundation.plugin(Toggler, 'Toggler')*/

// Tooltip
/*import { Tooltip } from 'foundation-sites/js/foundation.tooltip'
Foundation.plugin(Tooltip, 'Tooltip')*/

// Initialize
$(document).ready(() => {
	Foundation.addToJquery($);
	$(document).foundation();
})