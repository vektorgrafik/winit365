	self.init = function() {

		// Client JS check.
		$('html').removeClass('no-js');

		// Run resize events once on load.
		self.resizeActions();

		self.bindWindowEvents();

	}