import { throttle } from 'throttle-debounce'
import $ from 'jquery'

export default class Header {

	constructor() {
		this.lastScroll = 0
		this.elm = $('body')
		this.currentScroll = 0
		this.scrollOffset = 200
		this.headerHeight = $('header').height() 
	}

	init() {
		$(window).on('scroll', throttle(250, () => this.scrollEvent()));
	}

	scrollEvent() {

		this.currentScroll = $(window).scrollTop();

		if( this.currentScroll > this.headerHeight )
			this.elm.addClass('state--scrolled');
		else
			this.elm.removeClass('state--scrolled');
		
		if( this.currentScroll > this.lastScroll ) {
			
			this.elm.addClass('state--scrolled-down');
			this.elm.removeClass('state--scrolled-up');
			
		} else {
			
			if( this.currentScroll < this.lastScroll - 10 ) {
				this.elm.removeClass('state--scrolled-down');
				this.elm.addClass('state--scrolled-up');
			}

		}
		
		this.lastScroll = this.currentScroll;

		if( this.currentScroll >= ( $(window).height() - this.scrollOffset )) {
			this.elm.addClass('state--scrolled-past-height');
		} else {
			this.elm.removeClass('state--scrolled-past-height');
		}

	}

}