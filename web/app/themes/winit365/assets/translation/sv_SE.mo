��          �   %   �      0     1     8     N     e     k     p     u     {     �     �     �     �     �     �     �     �     
          )     /     F  Y   S  $   �  �  �  
   S     ^     o     �     �     �     �     �     �  "   �     �               &     2     7     J     V     o     w     �  Y   �     �                          	                    
                                                                                 (Edit) ... Read more &raquo; <h4>Related Posts</h4> First Last Menu Menus Oops, Post Not Found! Page Not Found Please activate some Widgets. Posts Categorized: Posts by %s Read Read more... Search Search Results for: Search for: Sorry, No Results. Tags: Try your search again. Type here... We could not find the page you were looking for. Try using our menu or do a search below. Your comment is awaiting moderation. Project-Id-Version: Vektorcore
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-09-07 10:20+0200
PO-Revision-Date: 
Last-Translator: Mathias Hellquist <mathias.hellquist@gmail.com>
Language-Team: fulgor <frag.fulgor@gmail.com>
Language: sv_SE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n
X-Poedit-Basepath: ../..
X-Generator: Poedit 2.0.3
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: vendor
X-Poedit-SearchPathExcluded-1: assets/js
X-Poedit-SearchPathExcluded-2: bower_components
X-Poedit-SearchPathExcluded-3: node_modules
 (Redigera) Läs mer &raquo; <h4>Relaterade inlägg</h4> Första Sista Meny Menyer Attans, hittade inget! Sidan kunde inte hittas Vänligen aktivera några Widgets. Kategoriserade inlägg: Inlägg av %s Läs mer Läs mer... Sök Sökresultat för: Sök efter: Tyvärr, inget resultat. Taggar: Testa en ny sökning. Skriv här… Vi kunde inte hitta sidan du försökte nå. Använd vår meny eller sök här nedanför. Din kommentar blir modererad. 