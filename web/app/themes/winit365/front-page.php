<?php

	get_header();
	
	get_template_part('templates/top', 'section');

	get_template_part('templates/loop', 'flexible');

	get_template_part('templates/flexible/financial-reports');

	?>

<?php get_footer(); ?>