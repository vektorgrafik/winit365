<?php get_header(); ?>

<?php get_template_part('templates/top', 'section'); ?>
			
<section class="post-section space--small">	
	<div class="grid-container">
		<div class="grid-x align-center">

		    <main id="main" class="large-8 medium-10 small-12 cell" role="main">
			    
		    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'templates/loop', 'single' ); ?>
				    
				<?php endwhile; ?>	

					<?php vektor_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'templates/content', 'missing' ); ?>
						
				<?php endif; ?>

			</main> <!-- end #main -->

		</div> <!-- end .grid-x -->
	</div>	<!-- end .grid-container -->
</section> <!-- end .post-section -->


<?php get_footer(); ?>