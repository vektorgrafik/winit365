<?php
/*
 * Template Name: Pressmeddelanden
 */

?>

<?php get_header(); ?>

	<?php get_template_part('templates/top', 'section'); ?>
	

	<?php 

		$post_parent_id = wp_get_post_parent_id( $post->ID );
		if ( $post_parent_id ) {

			$breadcrumb_args =  array(   
		        'parent_id' => $post->ID,
		        'include_front_page' => false,
		        'taxonomy' => false,
		        'wpml' => false,
		        'wrapper_class' => 'breadcrumb-section',
		        'separator' => '<div class="breadcrumb__separator">/</div>',
		        'override' => null
	   		);
			dazy_breadcrumbs($breadcrumb_args); 

		}

	?>

	<div class="standard-with-sidebar space--small">
		<div class="grid-container">

			<div class="grid-x grid-padding-x grid-padding-y align-justify">

				<main class="cell small-12 large-7">
			
					<?php 

						if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
						elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
						else { $paged = 1; }

						$press_args = array(

							'post_type' => 'pressrelease',
							'posts_per_page' => 5,

						);

						$press_query = new WP_Query($press_args);

						if ( $press_query->have_posts() ) {

							while ( $press_query->have_posts() ) { $press_query->the_post();

								get_template_part('templates/item-article');

							} wp_reset_postdata();
						}

					?>

				</main> <!-- end .cell -->

				<aside class="cell small-12 large-4 sidebar">
					<?php get_template_part('templates/loop', 'flexible-sidebar'); ?>
				</aside>

			</div>

		</div>
	</div>

	<?php 

		global $wp_query;
		$wp_query = $press_query;

		get_template_part('templates/pagination'); 

	?>

<?php get_footer(); ?>