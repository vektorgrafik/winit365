<form role="search" method="get" class="search__form" action="<?php echo home_url( '/' ); ?>">

	<label>
		<span class="show-for-sr"><?php echo _e( 'Search for:', 'label', 'vektor' ) ?></span>
		<input type="search" class="search__field" placeholder="<?php echo _e( 'Type here...', 'vektor' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo _e( 'Search for:', 'vektor' ) ?>" />
	</label>

	<input type="submit" class="search__submit button" value="<?php echo _e( 'Search', 'vektor' ) ?>" />

</form>