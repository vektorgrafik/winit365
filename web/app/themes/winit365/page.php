<?php 

	get_header();

	get_template_part('templates/top', 'section');

	$post_parent_id = wp_get_post_parent_id( $post->ID );
	if ( $post_parent_id ) {

		dazy_breadcrumbs([
			'wrapper_class' => 'breadcrumb-section',
			'include_front_page' => false,
			'separator' => '<span class="breadcrumb__separator">/</span>'
		]);
	}

	
?>

	<div class="standard">
		<div class="grid-container">

			<div class="grid-x grid-padding-x grid-padding-y">

				<main class="cell large-8 medium-10 small-12">
			
					<?php get_template_part('templates/loop', 'flexible'); ?>

				</main> <!-- end .cell -->

			</div>

		</div>
	</div>

<?php get_footer(); ?>