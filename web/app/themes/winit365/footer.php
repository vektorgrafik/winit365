				<footer class="page-footer space--small bg-image--fill" role="contentinfo">

					<div class="grid-container">
						<div class="grid-x grid-padding-x grid-padding-y align-center text-center">

							<?php if ( $phone = get_field('contact_phone', 'option') ) :
								$stripped_phone = str_replace(" ", "",$phone);
							 ?>
								<div class="cell small-12 large-3">
									<h4><?php echo __('Phone', 'dazy'); ?></h4>
									<a href="tel:<?=$stripped_phone;?>"><p><?=$phone;?></p></a>
								</div>

							<?php endif; ?>

							<?php if ( $address = get_field('contact_address', 'option') ) : ?>
								<div class="cell small-12 large-3">
									<h4><?php echo __('Location', 'dazy'); ?></h4>
									<p><?=$address;?></p>
								</div>

							<?php endif; ?>

							<?php if ( $mail = get_field('contact_mail', 'option') ) : ?>
								<div class="cell small-12 large-3">
									<h4><?php echo __('Contact', 'dazy'); ?></h4>
									<a href="mailto<?=$mail;?>"><p><?=$mail;?></p></a>
								</div>

							<?php endif; ?>

						</div>
					</div>

				</footer> <!-- end .footer -->

			</div>  <!-- end .off-canvas-content -->

		<?php wp_footer(); ?>

	</body>
	
</html> <!-- end page -->
