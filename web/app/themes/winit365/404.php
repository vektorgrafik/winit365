<?php get_header(); ?>
			
	<main id="main" role="main">

		<div class="row align-center space">

			<div class="small-12 medium-10 large-10 column">

				<h1 class="text-center"><?php _e( 'Page Not Found', 'vektor' ); ?></h1>

				<section class="entry-content entry-content--space text-center">
					<p><?php _e( 'We could not find the page you were looking for. Try using our menu or do a search below.', 'vektor' ); ?></p>
				</section> <!-- end article section -->

				<section class="search search--404 text-center">
				    <p><?php get_search_form(); ?></p>
				</section> <!-- end search section -->
		
		    </div>

		</div>

	</main> <!-- end #main -->

<?php get_footer(); ?>