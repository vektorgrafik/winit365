<?php get_header(); ?>

<section class="top-section top-section--archive">

	<h1 class="page-title"><?php the_archive_title();?></h1>
	<?php the_archive_description('<div class="taxonomy-description">', '</div>');?>

</section>

<section class="archive-section">	
	<div class="row">

	    <main id="main" class="large-8 medium-8 columns" role="main">
		    
	    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<!-- To see additional archive styles, visit the /parts directory -->
				<?php get_template_part( 'templates/loop', 'archive' ); ?>
			    
			<?php endwhile; ?>	

				<?php vektor_page_navi(); ?>
				
			<?php else : ?>
										
				<?php get_template_part( 'templates/content', 'missing' ); ?>
					
			<?php endif; ?>

		</main> <!-- end #main -->

		<aside class="sidebar column medium-4">
			<?php get_sidebar(); ?>
		</aside>

	</div>	<!-- end .row -->
</section> <!-- end .archive-section -->
	    

<?php get_footer(); ?>