<!doctype html>

  <html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">

		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) : ?>

			<!-- Icons & Favicons -->
			<link rel="apple-touch-icon" sizes="180x180" href="<?=IMAGES_FOLDER_URI?>favicon/apple-touch-icon.png">
			<link rel="icon" type="image/png" href="<?=IMAGES_FOLDER_URI?>favicon/favicon-32x32.png" sizes="32x32">
			<link rel="icon" type="image/png" href="<?=IMAGES_FOLDER_URI?>favicon/favicon-16x16.png" sizes="16x16">
			<link rel="manifest" href="<?=IMAGES_FOLDER_URI?>favicon/manifest.json">
			<link rel="mask-icon" href="<?=IMAGES_FOLDER_URI?>favicon/safari-pinned-tab.svg" color="#22dd99">
			<link rel="shortcut icon" href="<?=IMAGES_FOLDER_URI?>favicon/favicon.ico">
			<meta name="msapplication-config" content="<?=IMAGES_FOLDER_URI?>favicon/browserconfig.xml">
			<meta name="theme-color" content="#22dd99">

	    <?php endif; ?>

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        
		<?php
		wp_head();
		vg_google_tag_manager();
		?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		vg_google_tag_manager_ns();
		vg_google_analytics();
		?>

		<header class="page-header bg-white">
			<?php get_template_part( 'templates/nav', 'offcanvas-topbar' ); ?>
		</header>

		<div class="off-canvas position-right" id="off-canvas-nav" data-off-canvas>

			<div class="off-canvas__inner">
				<?php vektor_off_canvas_nav(); ?>
			</div>

		</div>

		<div class="off-canvas-content" data-off-canvas-content>
