<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
	<header class="article-header">	
		<h1 class="entry-title h2 single-title" itemprop="headline"><?php the_title(); ?></h1>
    </header> <!-- end article header -->

    <?php

    	// Fallback for pdf for now

    	$pdf_link = '#';
    	if ( $pdf_link ) {
    		echo '<a href="' . $pdf_link . '" class="button article__pdf-button"><i class="icon ion-android-document"></i>' . __('Öppna pdf', 'dazy') . '</a>';
    	}

    ?>
					
    <section class="entry-content" itemprop="articleBody">
		<?php the_post_thumbnail('full'); ?>
		<?php the_content(); ?>
	</section> <!-- end article section -->
																			
</article> <!-- end article -->