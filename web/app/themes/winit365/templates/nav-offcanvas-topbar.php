<div class="grid-container">
	<div class="grid-x grid-padding-x align-justify align-middle">

		<div class="cell shrink">
			<a href="<?php echo home_url(); ?>" class="page-header__logo logo"><?php bloginfo('name'); ?></a>
		</div> <!-- end .cell -->

		<div class="cell shrink">
			
			<?php vektor_top_nav(); ?>	

			<button data-toggle="off-canvas-nav" class="menu-toggle">
				<span class="menu-toggle__text show-for-sr">Meny</span>
				<span class="hamburger">
					<span class="hamburger__bar"></span>
					<span class="hamburger__bar"></span>
					<span class="hamburger__bar"></span>
				</span>
			</button>

		</div>  <!-- end .cell -->

	</div>  <!-- end .grid-x -->
</div> <!-- end .grid-container -->