<?php

	$article_date = date('Y-m-d', strtotime($post->post_date));
	$article_content = strip_tags( get_the_content() );
	$article_excerpt = dazy_excerpt($article_content, 155);


    $tags_to_strip = Array("strong","em","u","b");
    foreach ($tags_to_strip as $tag) {
        $article_excerpt = preg_replace("/<\\/?" . $tag . "(.|\\s)*?>/",'',$article_excerpt);
    }


	switch ( $post->post_type ) {
		case 'post':
			$categories = get_the_category();
			break;

		case 'pressrelease':
			$categories = get_the_terms($post, 'pressrelease_tax');
			break;

		case 'reports':
			$categories = get_the_terms($post, 'reports_tax');
			break;
		
		default:
			$categories = null;
			break;
	}

	if ( $categories ) {
		$categories = wp_list_pluck( $categories, 'name' );
		$categories = implode(', ', $categories);
	} else {
		$categories = '';
	}


?>

<article class="article">
	<p class="article__meta text-smaller text-stronger text-uppercase color-secondary"><?=$article_date;?><span class="article__meta-sep"></span><?php echo $categories; ?></p>
	<h2 class="article__title h3"><?php echo $post->post_title; ?></h2>
	<div class="entry-content">
		<p><?php echo  apply_filters( 'the_content', $article_excerpt ); ?></p>
	</div>
	<a href="<?php echo get_permalink(); ?>" class="button"><?php echo __('Läs hela', 'dazy'); ?></a>
</article>