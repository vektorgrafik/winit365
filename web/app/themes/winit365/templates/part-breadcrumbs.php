<section class="<?=$bc_wrapper_class?>">
	<div class="grid-container">
		<div class="grid-x">

			<div class="cell small-12">

				<nav class="breadcrumb">
					<?php echo ( isset( $bc_html )) ? $bc_html : null;?>
				</nav>

			</div>

		</div>		
	</div>
</section>