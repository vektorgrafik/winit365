<?php

	$post_args = array(
		'post_type' => 'post',
		'post_status' => 'published',
		'posts_per_page' => 2
	); 

	$post_query = new WP_Query($post_args);

	if ( $post_query->have_posts() ) : ?>


	<section class="latest-posts-section large-pull-top--small">
		<div class="grid-container grid-container--small">
			<div class="grid-x grid-padding-x grid-padding-y align-center">

				<?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
					<div class="cell small-12 medium-10 large-6">
						<div class="card">
							<p class="card__meta color-medium-gray text-uppercase"><?php echo get_the_date('d F'); ?></p>
							<h3 class="card__title"><a href="<?=get_permalink();?>" class="color-secondary"><?php echo $post->post_title; ?></a></h3>
						</div>
					</div> <!-- end .cell -->
				<?php endwhile; wp_reset_postdata(); ?>

			</div> <!-- end .grid-x -->
		</div> <!-- end .grid-container -->
	</section>

<?php endif; ?>