<?php

if ( have_rows('flexible_sidebar') ) while( have_rows('flexible_sidebar') ) : the_row();

	get_template_part( 'templates/sidebar/' . get_row_layout() );

endwhile;

?>