<?php

	global $post, $current_post_type;

	$tt = [
		'page' => 'Sidor',
		'post' => 'Nyheter',
	];

	if ( $current_post_type != $post->post_type ) {

		$current_post_type = $post->post_type;
		echo '<h2 class="h4 search__title">' . $tt[$post->post_type] . '</h2>';

	}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

	<p><a href="<?php the_permalink() ?>" class="link" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></p>

</article> <!-- end article -->
