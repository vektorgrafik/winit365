<?php

	$body = get_field('top_body') ?? null;
	$bg_type = get_field('top_bg_type') ?? null;
	$section_bg = '';

	$show_latest_posts = get_field('show_latest_posts') ? true : false;

	if ( is_singular( 'post' ) ) {

		// LINK TO PRESS PAGE
		global $wpdb;
		// Get post IDs of all pages using "template-press.php" 
		$press_pages = $wpdb->get_results("SELECT `post_id` FROM $wpdb->postmeta WHERE `meta_key` ='_wp_page_template' AND `meta_value` = 'template-press.php' ", ARRAY_A);
		// Get permalink using post ID of first page in the results 
		$press_id = $press_pages[0]['post_id'];

		$body = get_field('top_body', $press_id);
		$search_array = array('<h1>', '</h1>');
		$replace_array = array('<h2 class="h1">', '</h2>');

		$body = str_replace($search_array, $replace_array, $body);


	}

	if ( $body ) : 

		switch ( $bg_type ) {
			case 'none':
				# code...
				break;

			case 'image':
				$section_bg =  'style="background-image:url(' . get_field('top_bg_image')['url'] . ');"';
				break;
			
			default:
				# code...
				break;
		}

?>

	<section class="top-section bg-image--fill space" <?=$section_bg;?>>
		<div class="grid-container">
			<div class="grid-x grid-padding-x grid-padding-y align-center">
				<div class="cell small-12 medium-10 large-7"><?=$body;?></div>
			</div>
		</div>
	</section>

<?php endif; ?>

<?php 

	if ( $show_latest_posts === true ) : 

		get_template_part('templates/latest-posts-section'); 

	endif;

	if ( is_singular( 'post' ) ) {

		$breadcrumb_args =  array(   
	        'parent_id' => $press_id,
	        'include_front_page' => false,
	        'taxonomy' => false,
	        'wpml' => false,
	        'wrapper_class' => 'breadcrumb-section',
	        'separator' => '<div class="breadcrumb__separator">/</div>',
	        'override' => null
   		);
		dazy_breadcrumbs($breadcrumb_args); 

	}

?>