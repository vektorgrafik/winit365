<div class="stock-stat">
	<div class="stock-stat__inner">
		<?php

			// Different styling on .stock-stat__change depending on postive of negative change
			// If the trend is positive use class (modifier) .stock-stat__change--positive, if it is negative use .stock-stat__change negative

		?>
		<div class="stock-stat__change stock-stat__change--positive" id="stock-stat__change"></div>
		<div class="stock-stat__number" id="stock-stat__number"></div>
		<div class="stock-stat__text" id="stock-stat__text">WNT365 B</div>
	</div> <!-- end .stock-stat__inner -->
</div> <!-- end .stock-stat -->

<script>
    $.support.cors = true;

    $(function () {
        $.get('http://publish.ne.cision.com/papi/Ticker/60CE15B3BE5143FA829BBABE4F05BFDB', function(data){
            var positive = true;
            if ( data.Instruments[0].Quotes[0].DeltaPercentage.toString().charAt(0) == '-') {
                $('.stock-stat__change').removeClass('stock-stat__change--positive');
                $('.stock-stat__change').addClass('stock-stat__change--negative');
                positive = false;
            }

            $('#stock-stat__change').text( (positive ? '+' : '-') + (Math.round(data.Instruments[0].Quotes[0].DeltaPercentage * 1000) / 1000) + '%');

            $('#stock-stat__number').text(data.Instruments[0].Quotes[0].Price);
            $('#stock-stat__text').text(data.Instruments[0].TickerName);
        })
    });
</script>
