<?php
	
	$section_id = get_sub_field('section_id') ? 'id="' . get_sub_field('section_id') .'"' : '';
	$body = get_sub_field('body') ?? null;
	$cat = get_sub_field('people_tax');

?>

<section <?=$section_id;?> class="people-section bg-image--fill bg-white space--small relative">

	<?php get_template_part('templates/bg', 'triangle'); ?>

	<div class="grid-container">

		<div class="grid-x grid-padding-x grid-padding-y align-justify align-middle">
			<div class="cell small-12">
				<?=$body;?>
			</div> <!-- .cell -->
		</div>

		<?php

		// CALENDAR EVENTS 
		$people_args = array(
			'post_type' => 'people',
			'orderby' => 'menu_order',
			'order' => 'DESC',
			'posts_per_page' => -1,
			'tax_query' => array(
					array(
						'taxonomy' => 'people_tax',
						'field'    => 'term_id',
						'terms'    => $cat,
					),
				),
		);

		$people_query = new WP_Query($people_args);

		if ( $people_query->have_posts() ) : ?>
			
			<div class="grid-x grid-padding-x grid-padding-y">

				<?php while ( $people_query->have_posts() ) : $people_query->the_post(); 

					$name = get_field('name');
					$role = get_field('title');

					$attachment_id = get_post_thumbnail_id();
					$image = wp_get_attachment_image_src( $attachment_id, 'large' )[0];

				?>
					
					<div class="cell small-12 medium-6">
						<div class="person">
							<?php if ( $image ) : ?>
								<div class="person__image">
									<img src="<?=$image;?>" alt="<?php echo $name . ' image'; ?>">
								</div>
							<?php endif; ?>
							<h3 class="person__name color-secondary text-large clear-margin-b"><?=$name;?></h3>
							<p class="person__title text-large clear-margin-b"><?=$role;?></p>
						</div>
					</div>
				
				<?php endwhile; wp_reset_postdata(); ?>

			</div> <!-- end .grid-x -->

		<?php endif; ?>			

	</div> <!-- end .grid-container -->

</section>
