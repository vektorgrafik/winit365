<?php
	
	$section_id = get_sub_field('section_id') ? 'id="' . get_sub_field('section_id') .'"' : '';
	$body = get_sub_field('body') ?? null;

	if ( $body ) : 

?>

	<section <?=$section_id;?> class="stock-section bg-white space--small relative">
		
		<?php get_template_part('templates/bg', 'triangle'); ?>

		<div class="grid-container grid-container--small">
			<div class="grid-x grid-padding-x grid-padding-y align-justify align-middle">
				<div class="cell small-12 medium-6 large-7 entry-content"><?=$body;?></div>

				<div class="cell small-12 medium-6 large-4">

					<?php get_template_part('templates/part', 'stock-stat'); ?>

				</div> <!-- end .cell -->

			</div>
		</div>
	</section>

<?php endif; ?>
