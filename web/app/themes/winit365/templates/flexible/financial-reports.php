<?php

	
	$section_id = get_sub_field('section_id') ? 'id="' . get_sub_field('section_id') .'"' : '';
	$body = get_sub_field('body') ?? null;


	//FALLBACK

	$body = "<h2>Financial reports</h2>";

	if ( $body ) : 

?>

	<section <?=$section_id;?> class="reports-section bg-image--fill bg-white space--small relative">

		<?php get_template_part('templates/bg', 'triangle'); ?>

		<div class="grid-container">
			<div class="grid-x grid-padding-x grid-padding-y align-center">
				<div class="cell small-12 entry-content"><?=$body;?></div>
			</div>
		</div> <!-- end .grid-container -->

		<div class="grid-container">
			<div class="grid-x grid-padding-x grid-padding-y align-center">

				<div class="cell small-12 medium-10 large-6">
					<h3 class="h1 color-light-gray"><span class="color-primary">Q1</span> 2018</h3>
					<p>Presentation av delårsrapporten för Q1 2018.</p>
					<a href="#" class="button"><?php echo __('Read the report', 'dazy');?></a>
				</div>

				<div class="cell small-12 medium-10 large-6">
					<h3 class="h1 color-light-gray"><span class="color-primary">Q4</span> 2018</h3>
					<p>Presentation av delårsrapporten för Q4 2017.</p>
					<a href="#" class="button"><?php echo __('Read the report', 'dazy');?></a>
				</div>

			</div>
		</div> <!-- end .grid-container -->

	</section>

<?php endif; ?>
