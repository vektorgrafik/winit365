<?php
	
	$section_id = get_sub_field('section_id') ? 'id="' . get_sub_field('section_id') .'"' : '';
	$body = get_sub_field('body') ?? null;

	if ( $body ) : 

?>

	<section <?=$section_id;?> class="text-content-section space--small relative">
		
		<?php get_template_part('templates/bg', 'triangle'); ?>
		
		<div class="grid-container">
			<div class="grid-x grid-padding-x grid-padding-y align-center">
				<div class="cell small-12 entry-content"><?=$body;?></div>
			</div>
		</div>
	</section>

<?php endif; ?>
