<?php
	
	$section_id 	= get_sub_field('section_id') ? 'id="' . get_sub_field('section_id') .'"' : '';
	$body 			= get_sub_field('body') ?? null;
	$body_2 		= get_sub_field('body_2') ?? null;

	$post_count	 	= get_sub_field('post_count') ? get_sub_field('post_count') : 2 ; // How many posts to display. Default 2
	$post_type 		= get_sub_field('post_type') ? get_sub_field('post_type') : 'post'; // Which post_type/s to get posts from

	$show_all 		= get_sub_field('show_all') ? get_sub_field('show_all') : false; // Show all posts or use cat/tax
	$post_view_type = get_sub_field('post_view_type');

	switch ($post_view_type) {

		case 'full':
			$template_name = 'item-article';
			break;
		
		default:
			// or full
			$template_name = 'item-article--small';
			break;

	}

	if ( is_page_template( 'template-standard-sidebar.php' ) ) {
		$section_width = 'small-12';

	} else {
		$section_width = 'small-12 medium-8 large-6';	
	}

?>

<section <?=$section_id;?> class="post-section bg-image--fill bg-white space--small relative">

	<?php get_template_part('templates/bg', 'triangle'); ?>

	<div class="grid-container">
		<div class="grid-x grid-padding-x grid-padding-y align-justify align-middle">
		
			<div class="cell <?=$section_width;?>">

				<div class="entry-content">
					<?=$body;?>
				</div>

				<?php
					// CALENDAR EVENTS 
					$post_args = array(
						'post_type' => $post_type,
						'orderby' => 'post_date',
						'order' => 'DESC',
						'posts_per_page' => $post_count,
					);

					if ( $show_all === false ) {

						// Only get taxonomy if the matched post_type has been selected

						$post_cat 			= in_array('post', $post_type) ? get_sub_field('post_cat') : null;
						$pressrelease_tax 	= in_array('pressrelease', $post_type) ? get_sub_field('pressrelease_tax') : null;
						$reports_tax 		= in_array('reports', $post_type) ? get_sub_field('reports_tax') : null;

						// Place all in array
						$taxonomies = array($post_cat, $pressrelease_tax, $reports_tax);

						// Loop through array
						$i = 0; 

						$base_tax_args = [
							'tax_query' => array(
								'relation' => 'OR',
							),
						];

						foreach ( $taxonomies as $tax ) {

							if ( $tax ) { // Check that each value has an object and if they do add tax paramaters and merge them into original args for posts

								$term_ids = wp_list_pluck( $tax, 'term_id' );
								$taxonomy = $tax[0]->taxonomy;

								$base_tax_args['tax_query'][] = array(
									'taxonomy' => $taxonomy,
									'field'    => 'term_id',
									'terms'    => $term_ids,
								);


							$i++;}  // end if ( $tax )

						}  // end foreach ( $taxonomies as $tax )

						if ( $i > 0 ) { // Meaning at least one succesfull tax exists

							$post_args = array_merge($post_args, $base_tax_args);

						} // end if ( $i > 0 )

					} // end if ( $show_all === false )

					$post_query = new WP_Query($post_args);

					if ( $post_query->have_posts() ) : 	?>
						
						<div class="grid-container full space--smaller">
							<?php while ( $post_query->have_posts() ) : $post_query->the_post(); 

								get_template_part('templates/' . $template_name);

							endwhile; wp_reset_postdata(); ?>
						</div> <!-- end .grid-container -->

					<?php endif; ?>

				<?php if ( $body_2 ) : ?>
					<div class="entry-content">
						<?=$body_2;?>
					</div>
				<?php endif; ?>
				
			</div> <!-- end .cell -->
		</div> <!-- end .grid-x -->
	</div> <!-- end .grid-container -->

</section>
