<?php
	
	$section_id = get_sub_field('section_id') ? 'id="' . get_sub_field('section_id') .'"' : '';
	$body = get_sub_field('body') ?? null;
	$calendar_image = get_sub_field('calendar_image') ?? null;
	$calendar_event_count = get_sub_field('calendar_event_count') ?? 1;
	$calendar_event_order = get_sub_field('calendar_event_order') ? get_sub_field('calendar_event_order') : 'title_first';
	$todays_date = date("Ymd");

	switch ($calendar_event_order) {
		case 'title_first':
			$event_class =  'event--spaced';
			break;
		
		default:
			$event_class =  '';
			break;
	}

	if ( $calendar_image ){
		$cell_class = 'medium-8 large-6';
	} else {
		$cell_class = '';
	}

?>

<section <?=$section_id;?> class="calendar-section bg-image--fill bg-white space--small relative">
	
	<?php get_template_part('templates/bg', 'triangle'); ?>
	
	<div class="grid-container">
		<div class="grid-x grid-padding-x grid-padding-y align-justify align-middle">
			
			<?php if ( $calendar_image ) : ?>
				
				<div class="cell small-12 medium-4 large-6">
					<img src="<?=$calendar_image['sizes']['large'];?>" alt="<?=$calendar_image['alt'];?>" class="xlarge-pull-left">
				</div>

			<?php endif ;?>

			<div class="cell small-12 <?=$cell_class;?>">

				<div class="entry-content">
					<?=$body;?>
				</div>

				<?php
					// CALENDAR EVENTS 
					$calendar_args = array(
						'post_type' => 'calendar',
						'orderby' => 'meta_value',
						'order' => 'ASC',
						'posts_per_page' => $calendar_event_count,
						'meta_query' => array(
					        'state_clause' => array(
					            'key' => 'event_date',
					            'value' => $todays_date,
					            'compare' => '>=',
					        ),
					    ),
   
					);

					$calendar_query = new WP_Query($calendar_args);

					if ( $calendar_query->have_posts() ) : 	?>
						
						<ul class="list list--clean list--calendar">
							<?php while ( $calendar_query->have_posts() ) : $calendar_query->the_post(); 

								$event_title = get_field('event_title');
								$event_date = get_field('event_date');
								$event_date_formatted = date('Y-m-d', strtotime($event_date));

							?>
								<li class="list__item event <?=$event_class;?>">
									<span class="event__date color-light-medium-gray text-default"><?=$event_date_formatted;?></span>
									<span class="event__title color-secondary text-large"><?=$event_title;?></span>
								</li>

							<?php endwhile; wp_reset_postdata(); ?>
						</ul>

					<?php endif; ?>
				
			</div> <!-- end .cell -->
		</div> <!-- end .grid-x -->
	</div> <!-- end .grid-container -->

</section>
