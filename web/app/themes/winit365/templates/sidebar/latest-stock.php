<?php 

	$body = get_sub_field('body');

?>

<div class="sidebar__block">
	
	<div class="grid-container">
		<div class="grid-x grid-padding-y">
			<div class="cell">
				<?php get_template_part('templates/part', 'stock-stat'); ?>
			</div>

			<?php if ( $body ) : ?>
				<div class="cell entry-content">
					<?php echo $body; ?>
				</div>
			<?php endif; ?>
		</div> <!-- end .grid-x -->
	</div>  <!-- end .grid-container -->
</div>