<?php
	
	$body = get_sub_field('body');
	$docs_repeater = get_sub_field('document_repeater');

	if ( $docs_repeater ) : 

?>

	<div class="sidebar__block sidebar__block--padded">
		
		<?php echo $body; ?>

		<ul class="list list--clean list--docs entry-content">
			<?php foreach ( $docs_repeater as $doc ) : 

				$file = $doc['file'];
				$text = $doc['text'] ? $doc['text'] : $doc['title'];

			?>

				<li class="list__item"><a href="<?=$file['url'];?>" alt="<?=$file['alt'];?>" class="text-small"><?=$text;?></a></li>

			<?php endforeach; ?>
		</ul>

	</div> <!-- end .sidebar__block -->

<?php endif; ?>