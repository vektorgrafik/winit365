<?php
	
	$body = get_sub_field('body');
	$contact_persons = get_sub_field('contact_person');

?>

<div class="sidebar__block sidebar__block--padded">
	
	<?php echo $body; ?>

	<?php foreach ( $contact_persons as $person ) : 

		$name = get_field('name', $person->ID);
		$title = get_field('title', $person->ID);
		$phone = get_field('phone', $person->ID);
		$phone_stripped = str_replace(" ", "", $phone);
		$mail = get_field('mail', $person->ID);

	?>

		<h4><?=$name;?></h4>
		<p class="text-small"><?=$title;?></p>
		
		<div class="entry-content">
			<a href="<?='tel:' . $phone_stripped;?>"><?=$phone;?></a>
			<a href="<?='mailto:' . $mail;?>"><?=$mail;?></a>
		</div>

	<?php endforeach; ?>

</div> <!-- end .sidebar__block -->