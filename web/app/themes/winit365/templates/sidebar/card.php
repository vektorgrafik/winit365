<?php
	
	$body = get_sub_field('body');

?>

<div class="sidebar__block">
	<div class="card card--padded">
		<?php echo $body; ?>
	</div> <!-- end .card -->
</div>