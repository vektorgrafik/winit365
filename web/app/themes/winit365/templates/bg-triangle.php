<?php 

	$section_bg = get_sub_field('section_bg');

	switch ($section_bg) {
		case 'none':
			$bg = '';
			break;

		case 'triangle_left':
			$bg = '<div class="bg-triangle bg-triangle--left"></div>';
			break;

		case 'triangle_right':
			$bg = '<div class="bg-triangle bg-triangle--right"></div>';
			break;
		
		default:
			$bg = '';
			break;
	}

	echo $bg;

?>