<?php 
	$title = $post->post_title;
	$permalink = get_permalink();
	$post_date = $post->post_date;
	$post_date_formatted = date('Y-m-d', strtotime($post_date));

?>

<div class="grid-x align-bottom">
	<div class="cell auto"><a href="<?=$permalink;?>" class="color-secondary"><p class="text-large"><?=$title;?></p></a></div>
	<div class="cell shrink"><p class="color-light-medium-gray text-default align-right"><?=$post_date_formatted;?></p></div>
</div>