<?php

define('CLASS_FOLDER', get_template_directory() . '/classes/' );
define('ASSETS_FOLDER', get_template_directory() . '/assets/' );
define('IMAGES_FOLDER', ASSETS_FOLDER . '/images/' );
define('FUNCTIONS_FOLDER', get_template_directory() . '/functions/' );
define('TRANSLATIONS_FOLDER', ASSETS_FOLDER . '/translation/' );
define('IMAGES_FOLDER_URI', get_template_directory_uri() . '/assets/images/' );

// Configuration and constants
require_once( FUNCTIONS_FOLDER . 'configuration.php' );

// Engage output-buffering
require_once ( FUNCTIONS_FOLDER . 'outputbuffer.php' );

// Various helper functions
require_once( FUNCTIONS_FOLDER . 'helpers.php' ); 

// Theme support options
require_once( FUNCTIONS_FOLDER . 'theme-support.php' ); 

// WP Head and other cleanup functions
require_once( FUNCTIONS_FOLDER . 'cleanup.php' ); 

// Register scripts and stylesheets
require_once( FUNCTIONS_FOLDER . 'enqueue-scripts.php' ); 

// Register custom menus and menu walkers
require_once( FUNCTIONS_FOLDER . 'menu.php' ); 
require_once( FUNCTIONS_FOLDER . 'menu-walkers.php' ); 

// Register sidebars/widget areas
require_once( FUNCTIONS_FOLDER . 'sidebar.php' ); 

// Makes WordPress comments suck less
require_once( FUNCTIONS_FOLDER . 'comments.php' ); 

// Replace 'older/newer' post links with numbered navigation
require_once( FUNCTIONS_FOLDER . 'page-navi.php' ); 

// Adds support for multiple languages
require_once( TRANSLATIONS_FOLDER . 'translation.php' ); 

// Adds site styles to the WordPress editor
require_once( FUNCTIONS_FOLDER . 'editor-styles.php'); 

// Related post function - no need to rely on plugins
//require_once( FUNCTIONS_FOLDER . '/related-posts.php'); 

// Use this as a template for custom post types
require_once( FUNCTIONS_FOLDER . 'custom-post-type.php' );

// Customize the WordPress login menu
require_once( FUNCTIONS_FOLDER . 'login.php' ); 

// ACF Layout Templates
require_once( FUNCTIONS_FOLDER . 'acf-templates.php' ); 

// Customize the WordPress admin
require_once( FUNCTIONS_FOLDER . 'admin.php' );


//Import pressreleases

add_action( 'init', 'cision_pressreleases_cron_url' );

require_once('functions/class.cision-import.php');
function cision_pressreleases_cron_url(){
    global $cision_import;

    if( isset($_GET['sync_cision_pressreleases']) ):
        $cision_import::sync_cision_posts();
    endif;

}