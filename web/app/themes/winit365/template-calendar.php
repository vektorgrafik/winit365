<?php
/*
 * 
 */

?>

<?php get_header(); ?>

	<?php get_template_part('templates/top', 'section'); ?>
	

	<?php 

		$post_parent_id = wp_get_post_parent_id( $post->ID );
		if ( $post_parent_id ) {

			$breadcrumb_args =  array(   
		        'parent_id' => $post->ID,
		        'include_front_page' => false,
		        'taxonomy' => false,
		        'wpml' => false,
		        'wrapper_class' => 'breadcrumb-section',
		        'separator' => '<div class="breadcrumb__separator">/</div>',
		        'override' => null
	   		);
			dazy_breadcrumbs($breadcrumb_args); 

		}

	?>

	<div class="standard-with-sidebar">
		<div class="grid-container">

			<div class="grid-x grid-padding-x grid-padding-y align-justify">

				<main class="cell small-12 large-7">
			
					<?php get_template_part('templates/loop', 'flexible'); ?>

				</main> <!-- end .cell -->

				<aside class="cell small-12 large-4 sidebar">
					<?php get_template_part('templates/loop', 'flexible-sidebar'); ?>
				</aside>

			</div>

		</div>
	</div>

<?php get_footer(); ?>