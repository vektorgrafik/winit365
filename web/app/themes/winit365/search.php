<?php get_header(); ?>

	<main id="main" role="main">

		<div class="row align-center space">

			<div class="small-12 medium-9 column">

				<h1 class="h2 text-center"><?php _e( 'Search Results for:', 'vektor' ); ?> <?php echo esc_attr(get_search_query()); ?></h1>

				<section class="search text-center">
					<p><?php get_search_form(); ?></p>
				</section> <!-- end search section -->

				<div class="search__results">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php get_template_part( 'templates/loop', 'searchresult' ); ?>

					<?php endwhile; ?>

				</div>

					<?php vektor_page_navi(); ?>

				<?php else : ?>

					<?php get_template_part( 'templates/content', 'missing' ); ?>

			    <?php endif; ?>

		    </div>

		</div>

	</main> <!-- end #main -->

<?php get_footer(); ?>
