<?php
// This file handles the admin area and functions - You can use this file to make changes to the dashboard.

function vg_table_views() {
	
	require_once CLASS_FOLDER . '/VgTableView/VgTableConfig.php';
	require_once CLASS_FOLDER . '/VgTableView/VgTableView.php';
	
	$config 	= new VgTableConfig;
	$view 		= new VgTableView;
	$config2 	= new VgTableConfig;
	$view2 		= new VgTableView;

	$config->table('wp_options',
		[
			[ 'name' => 'option_id', 			'title' => 'Option Id', 			'type' => 'int'],
			[ 'name' => 'option_name', 			'title' => 'Option Name', 			'type' => 'string' ],
			[ 'name' => 'option_value', 		'title' => 'Option Value', 			'type' => 'string' ],
		])
		->menuOrder(20)
		->rowsPerPage(25)
		->menuTitle('Fluff')
		->icon('media-spreadsheet')
		->order('option_id', 'asc')
		->description('Lista över samtliga registrerade serienummer');

	$view->create( $config );

}
// add_action( 'admin_menu', 'vg_table_views' );
// add_action( 'wp_ajax_get_vg_table_data', 'vg_table_views' );

// Filter Template: 
# add_filter('VgTableColumn_option_name', 'fluff', 1, 10);



/************* DASHBOARD WIDGETS *****************/
// Disable default dashboard widgets
function disable_default_dashboard_widgets() {
	
	// Remove_meta_box('dashboard_right_now', 'dashboard', 'core');    // Right Now Widget
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'core'); // Comments Widget
	remove_meta_box('dashboard_incoming_links', 'dashboard', 'core');  // Incoming Links Widget
	remove_meta_box('dashboard_plugins', 'dashboard', 'core');         // Plugins Widget

	// Remove_meta_box('dashboard_quick_press', 'dashboard', 'core');  // Quick Press Widget
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'core');   // Recent Drafts Widget
	remove_meta_box('dashboard_primary', 'dashboard', 'core');         //
	remove_meta_box('dashboard_secondary', 'dashboard', 'core');       //

	// Removing plugin dashboard boxes
	remove_meta_box('yoast_db_widget', 'dashboard', 'normal');         // Yoast's SEO Plugin Widget

}

// RSS Dashboard Widget
function vektor_rss_dashboard_widget() {
	if(function_exists('fetch_feed')) {
		include_once(ABSPATH . WPINC . '/feed.php');               	// include the required file
		$feed = fetch_feed('http://vektor.com/feed/rss/');			// specify the source feed
		$limit = $feed->get_item_quantity(5);                      	// specify number of items
		$items = $feed->get_items(0, $limit);                     	// create an array of items
	}
	if ($limit == 0) echo '<div>' . __( 'The RSS Feed is either empty or unavailable.', 'vektor' ) . '</div>';   // fallback message
	else foreach ($items as $item) { ?>

	<h4 style="margin-bottom: 0;">
		<a href="<?php echo $item->get_permalink(); ?>" title="<?php echo mysql2date(__('j F Y @ g:i a', 'vektor'), $item->get_date('Y-m-d H:i:s')); ?>" target="_blank">
			<?php echo $item->get_title(); ?>
		</a>
	</h4>
	<p style="margin-top: 0.5em;">
		<?php echo substr($item->get_description(), 0, 200); ?>
	</p>
	<?php }
}


// Calling all custom dashboard widgets
function vektor_custom_dashboard_widgets() {

	// wp_add_dashboard_widget('vektor_rss_dashboard_widget', __('Custom RSS Feed (Customize in admin.php)', 'vektor'), 'vektor_rss_dashboard_widget');

}
// removing the dashboard widgets
add_action('admin_menu', 'disable_default_dashboard_widgets');

// adding any custom widgets
add_action('wp_dashboard_setup', 'vektor_custom_dashboard_widgets');

/************* CUSTOMIZE ADMIN *******************/
// Custom Backend Footer
function vektor_custom_admin_footer() {
	_e('<span id="footer-thankyou"><a href="http://www.vektorgrafik.se" target="_blank">Vektorgrafik</a></span>.', 'vektor');
}

// adding it to the admin area
add_filter('admin_footer_text', 'vektor_custom_admin_footer');

// Sanitize uploaded file names
function vektor_sanitize_file_name( $filename ) {
	$filename = str_replace( array( 'å', 'ä', 'Å', 'Ä' ), 'a', $filename );
	$filename = str_replace( array( 'ö', 'Ö' ), 'o', $filename );
	
	$filename = preg_replace( '/[^a-zA-Z0-9\_\-\.]+/i', '', $filename );
	
	$filename = mb_strtolower( $filename );
	
	return $filename;
}
add_filter('sanitize_file_name', 'vektor_sanitize_file_name', 10, 1);

/* No more folders in uploads directory */
function vektor_theme_activation() {
	update_option('uploads_use_yearmonth_folders', 0);
}
add_action( 'after_switch_theme', 'vektor_theme_activation');
add_filter( 'option_uploads_use_yearmonth_folders', '__return_false', 100 );

/*
 * ACF option pages 
 */

if ( function_exists('acf_add_options_page') ) {

	acf_add_options_page('Övriga inställningar');
	
}

/*
 * Custom ACF - toolbar primarly for section titles
 */

function dazy_custom_toolbar( $toolbars )
{

	$toolbars['Custom [Dazy]' ] = array();
	$toolbars['Custom [Dazy]' ][1] = array( 'styleselect', 'bold', 'italic', 'color_tertiary', 'link', 'bullist', 'numlist', 'blockquote', 'removeformat');

	// return $toolbars - IMPORTANT!
	return $toolbars;
}
add_filter( 'acf/fields/wysiwyg/toolbars' , 'dazy_custom_toolbar'  );

/*
 * Adding custom formats to TinyMCE editor.
 */

function vektor_tiny_mce_before_init( $settings ){
	
	$style_formats = [

		[
		
			'title' => 'Textformat (SEO)',
            'items' => [
                [
                    'title' => 'Brödtext',
                    'selector' => 'p',  
                ], 
                [
                    'title' => 'Rubrik 1 (endast 1 på sida)',
                    'format' => 'h1',  
                ],
                [
                    'title' => 'Rubrik 2',
                    'format' => 'h2',  
                ],
                [
                    'title' => 'Rubrik 3',
                    'format' => 'h3',  
                ],
                [
                    'title' => 'Rubrik 4',
                    'format' => 'h4',  
                ],
                [
                    'title' => 'Rubrik 5',
                    'format' => 'h5',  
                ],
            ]

		],
		[
		
			'title' => 'Alternativt textformat (utseende)',
            'items' => [
                [
                    'title' => 'Utseende - Liten text',
                    'selector' => 'p',  
                    'classes' => 'text-small',
                ], 
            	[
                    'title' => 'Utseende - Stor text/Ingress',
                    'selector' => 'p',  
                    'classes' => 'text-large',
                ],
                [
                    'title' => 'Utseende - Rubrik 1',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'h1',
                ],
                [
                    'title' => 'Utseende - Rubrik 2',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'h2',
                ],
                [
                    'title' => 'Utseende - Rubrik 3',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'h3',
                ],
                [
                    'title' => 'Utseende - Rubrik 4',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'h4',
                ],
                [
                    'title' => 'Utseende - Rubrik 5',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'h5',
                ],
            ]

		],
        [
        
            'title' => 'Textfärg',
            'items' => [
                [
                    'title' => 'Primärfärg',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'color-primary',
                ],
                [
                    'title' => 'Sekundärfärg',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'color-secondary',
                ]
            ]

        ],
		[
		
			'title' => 'Justering',
            'items' => [

                [
                    'title' => 'Vänsterställ',
                    'icon'	=> 'alignleft',
                    'block' => 'div',  
                    'classes' => 'text-left',
                    'wrapper' => true,
                ],
                [  
                    'title' => 'Centrera',  
                    'icon'	=> 'aligncenter',
                    'block' => 'div',
                    'classes' => 'text-center',
                    'wrapper' => true
                ],
                [  
                    'title' => 'Högerställ',  
                    'icon'	=> 'alignright',
                    'block' => 'div',
                    'classes' => 'text-right',
                    'wrapper' => true
                ],
                [
                    'title' => 'Ta bort marginal (bottom)',
                    'selector' => 'p,h1,h2,h3,h4,h5,h6',  
                    'classes' => 'clear-margin-b'
                ],
              
            ]

		],
        [
        
            'title' => 'Länkar',
            'items' => [

                [
                    'title' => 'Länk',
                    'selector' => 'a',  
                    'classes' => 'link'
                ],
                [
                    'title' => 'Länk med pil',
                    'selector' => 'a',  
                    'classes' => 'link link--arrow-r'
                ],
            ]

        ],
        [
        
            'title' => 'Punktlistor',
            'items' => [

                [
                    'title' => 'Lista med talstreck',
                    'selector' => 'ul',  
                    'classes' => 'list list--lines'
                ]
            ]

        ]

	];

	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
	
}

add_filter('tiny_mce_before_init', 'vektor_tiny_mce_before_init');