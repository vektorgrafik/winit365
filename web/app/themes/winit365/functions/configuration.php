<?php

/*
|--------------------------------------------------------------------------
| Version number
|--------------------------------------------------------------------------
|
| Global version number, bump this whenever a change has been
| made to either javascript or scss.
|
*/
define( 'VG_VERSION', '1.0.0' );


/*
|--------------------------------------------------------------------------
| Environment
|--------------------------------------------------------------------------
|
| development = Unminified css and javascript, with sourcemaps.
| production = Minified files without sourcemaps.
|
*/
define( 'ENVIRONMENT', 'development' );



/*
|--------------------------------------------------------------------------
| Google Analytics, Tag Manager and Google Maps API Key
|--------------------------------------------------------------------------
|
| Uncomment and fill in Analytics and TagManager IDs here
|
*/
// define( 'VG_GOOGLE_ANALYTICS_ID', '' );
// define( 'VG_GOOGLE_TAG_MANAGER_ID', '' );
// define( 'VG_GOOGLE_MAPS_API_KEY', '' );
