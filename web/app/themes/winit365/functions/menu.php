<?php
// Register menus
register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu', 'vektor' ),   // Main nav in header
		'footer-links' => __( 'Footer Links', 'vektor' ) // Secondary nav in footer
	)
);

// The Top Menu
function vektor_top_nav() {
	 wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'menu_class' => 'menu menu--main vertical medium-horizontal',       // Adding custom nav class
        'items_wrap' => '<ul id="%1$s" class="%2$s" data-responsive-menu="accordion medium-dropdown">%3$s</ul>',
        'theme_location' => 'main-nav',        			// Where it's located in the theme
        'depth' => 2,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Topbar_Menu_Walker()
    ));
} /* End Top Menu */

// The Off Canvas Menu
function vektor_off_canvas_nav() {
	 wp_nav_menu(array(
        'container' => false,                           // Remove nav container
        'menu_class' => 'menu menu--offcanvas vertical',                // Adding custom nav class
        'items_wrap' => '<ul id="%1$s" class="%2$s" data-accordion-menu>%3$s</ul>',
        'theme_location' => 'main-nav',        			// Where it's located in the theme
        'depth' => 2,                                   // Limit the depth of the nav
        'fallback_cb' => false,                         // Fallback function (see below)
        'walker' => new Off_Canvas_Menu_Walker()
    ));
} /* End Off Canvas Menu */

// The Footer Menu
function vektor_footer_links() {
    wp_nav_menu(array(
    	'container' => 'false',                         // Remove nav container
    	'menu' => __( 'Footer Links', 'vektor' ),   	// Nav name
    	'menu_class' => 'menu',      					// Adding custom nav class
    	'theme_location' => 'footer-links',             // Where it's located in the theme
        'depth' => 0,                                   // Limit the depth of the nav
    	'fallback_cb' => ''  							// Fallback function
	));
} /* End Footer Menu */

// Header Fallback Menu
function vektor_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => '',      // Adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // Before each link
        'link_after' => ''                             // After each link
	) );
}

// Footer Fallback Menu
function vektor_footer_links_fallback() {
	/* You can put a default here if you like */
}


/*
 * ------------------------------------------------------------
 * Vektor Breadcrumbs function
 * 0.9.1, still needs a bit of love before completely complete.
 * ------------------------------------------------------------
*/

function dazy_breadcrumbs( $args = [] ) {

    global $post, $wp_query;

    $args = wp_parse_args($args, array(
        
        'parent_id' => $post->ID,
        'include_front_page' => true,
        'taxonomy' => false,
        'wpml' => false,
        'wrapper_class' => false,
        'separator' => null,
        'override' => null

    ));

    $bc_html = "";
    $bc_wrapper_class = $args['wrapper_class'];
    
    if( $args['wpml'] == true )
        $args['parent_id'] = icl_object_id( $args['parent_id'], 'page', true, ICL_LANGUAGE_CODE );

    $ancestor   = get_post( $args['parent_id'] );
    $ancestors  = get_post_ancestors( $args['parent_id'] );

    // Get Home Post
    $home = get_option( 'page_on_front' );
    if( $home )
        $home = get_post( $home );

    // Include front page if defined
    $start_page = ( $args['include_front_page'] ) ? '<a class="breadcrumb__link breadcrumb__link--home" href="' . get_permalink( $home->ID ) . '">' . $home->post_title . '</a>' . $args['separator'] : null; // TO DO: make dynamic.

    // Check for override
    if( $args['override'] ) {
        
        $bc_html = $start_page . '<a class="breadcrumb__link is-active" href="#">' . $args['override'] . '</a>';

        // Get template and spit it out.
        ob_start();
        include get_template_directory() . '/templates/part-breadcrumbs.php';
        echo ob_get_clean();
        return;
    }


    // Loop out all parents
    if( is_array( $ancestors )) {
        
        // Reverse post order
        $ancestors = array_reverse( $ancestors );

        // Loop through and output
        $i = 0;
        foreach( $ancestors as $page ) {

            $bc_html .= '<a class="breadcrumb__link" href="' . get_permalink( $page ) . '">' . get_the_title( $page ) . '</a>';
            
            if( $i < count( $ancestors ))
                $bc_html .= $args['separator'];

            $i++;

        }
    }

    // Attach closest parent
    if( $args['parent_id'] !== $post->ID )
        $bc_html .= '<a class="breadcrumb__link" href="' . get_permalink( $ancestor->ID ) . '">' . get_the_title( $ancestor ) . '</a>' . $args['separator'];

    // If category, then attach that, else just get current page.
    if( $args['taxonomy'] ) : 
        
        $curr_term_ID   = $wp_query->get_queried_object_id();
        $ancestors      = get_ancestors( $curr_term_ID, $args['taxonomy'] );

        // Check if we're on a tax page, if we're not, then let's try to locate
        // where we are and help out a bit.
        if( ! is_tax() ) :

            // The post_type we're on is definitely in the tax we're getting.
            if( is_object_in_taxonomy( $post->post_type, $args['taxonomy'] )) :

                $term_tree = get_the_terms( $post, $args['taxonomy'] );

                $ancestors = wp_list_pluck( $term_tree, 'term_id' );

            endif;

        else : 
            
            $ancestors = get_ancestors( $curr_term_ID, $args['taxonomy'] );
            $ancestors = array_reverse( $ancestors );

        endif;

        if( empty( $ancestors )) : 

            $curTax = get_term( $curr_term_ID, $args['taxonomy'] );
            $bc_html .= '<a class="breadcrumb__link is-active" href="' . get_category_link( $curTax->term_id ) . '">' . $curTax->name . '</a>';

        else : 

            $curTax = get_term( $curr_term_ID, $args['taxonomy'] );

            $i = 0;
            foreach( $ancestors as $ancestor_id ) : 

                $parentTax = get_term( $ancestor_id, $args['taxonomy'] );
                $bc_html .= '<a class="breadcrumb__link" href="' . get_category_link( $parentTax->term_id ) . '">' . $parentTax->name . '</a>';               

                if( $i < count( $ancestors ))
                    $bc_html .= $args['separator'];                

                $i++;

            endforeach;

            // Only attach term if we're on a taxonomy page.
            // We already have the final one if we're on a post_type page.
            if( is_tax() )
                $bc_html .= '<a class="breadcrumb__link is-active" href="' . get_category_link( $curTax->term_id ) . '">' . $curTax->name . '</a>';         

        endif;

        // Attach the actual post at the end.
        if( ! is_tax() ) : 
            $bc_html .= '<a class="breadcrumb__link is-active" href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a>';            
        endif;
   
    else :

        $bc_html .= '<a class="breadcrumb__link is-active" href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a>';

    endif;

    $bc_html = $start_page . $bc_html;

    // Get template and spit it out.
    ob_start();
    include get_template_directory() . '/templates/part-breadcrumbs.php';

    echo ob_get_clean();

}

/*
* -------------------------------------------------------------
*/