<?php
// Numeric Page Navi (built into the theme by default)
function vektor_page_navi($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
	echo $before.'<nav class="page-navigation"><ul class="pagination">'."";
	if ($start_page >= 2 && $pages_to_show < $max_page) {
		$first_page_text = __( 'First', 'vektor' );
		echo '<li><a href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
	}
	echo '<li>';
	previous_posts_link( __('Previous', 'vektor') );
	echo '</li>';
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="current"> '.$i.' </li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li>';
	next_posts_link( __('Next', 'vektor' ));
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = __( 'Last', 'vektor' );
		echo '<li><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
	}
	echo '</ul></nav>'.$after."";
} /* End page navi */


/* Vektor Page Navi 2 Beta
/*
/* This code is from an unknown civilisation outside our solar system.
/* It's was found buried deep in an arctic glacier by norwegian discoverers in 1910.
/*
/* Adapted to Wordpress by Magnus */

function vektor_page_navi_2( $args = [] ) {

	$args = wp_parse_args( $args, [
		'mid_size' => 2,
		'parent_class' => 'pagination-navigation',
		'nav_item_class' => 'nav-item',
		'current_page_class' => 'current',
		'first_page_text' => __('First', 'vektor'),
		'last_page_text' => __('Last', 'vektor')
  	]);


	global $wpdb, $wp_query;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = (get_query_var('paged')) ? intval(get_query_var('paged')) : 1;

	$max_num_pages = $wp_query->max_num_pages;

	$nav_item_classes = $args['parent_class'] . '__item ' . $args['nav_item_class'] . ' ' . $args['nav_item_class'];

	if ($max_num_pages > 1) {

		$start_page = ($paged - $args['mid_size'] <= 1) ? 1 : $paged - $args['mid_size'];
		$end_page = ($paged + $args['mid_size'] >= $max_num_pages) ? $max_num_pages : $paged + $args['mid_size'];

		echo '<ul class="' . $args['parent_class'] . '">';

		// First page
		if ($paged > 2) {
			echo '<li class="' . $nav_item_classes . '--first';
			if ($paged == 1) { echo ' ' . $args['nav_item_class'] . '--' . $args['current_page_class'] . ' ' . $args['current_page_class']; }
			echo '"><a href="' . untrailingslashit(get_pagenum_link()) . '" class="' . $args['nav_item_class'] . '__link">' . $args['first_page_text'] . '</a></li>';
		}

		$i = $start_page;

		if ($start_page >= 2) {
			echo '<li class="' . $nav_item_classes . '--space">...</li>';
		}

		// Prev page
		if ($paged != 1) {
			echo '<li class="' . $nav_item_classes . '--prev"><a href="' . untrailingslashit(get_pagenum_link($paged - 1)) . '" class="' . $args['nav_item_class'] . '__link">&larr;</a></li>';
		}

		// Loop through midsize pages
		while ($i >= $start_page && $i <= $end_page) {
			echo '<li class="' . $nav_item_classes . '--' . $i;
			if ($i == $paged) { echo ' ' . $args['nav_item_class'] . '--' . $args['current_page_class'] . ' ' . $args['current_page_class']; }
			echo '"><a href="' . untrailingslashit(get_pagenum_link($i)) . '" class="' . $args['nav_item_class'] . '__link">' . $i . '</a></li>';
			$i++;
		}

		// Next page
		if ($paged != $max_num_pages) {
			echo '<li class="' . $nav_item_classes . '--next"><a href="' . untrailingslashit(get_pagenum_link($paged + 1)) . '" class="' . $args['nav_item_class'] . '__link">&rarr;</a></li>';
		}

		if ($end_page <= $max_num_pages - 1) {
			echo '<li class="' . $nav_item_classes . '--space">...</li>';
		}

		// Last page
		if ($paged != $max_num_pages && $end_page < $max_num_pages) {
			echo '<li class="' . $nav_item_classes . '--last';
			if ($paged == $max_num_pages) { echo ' ' . $args['nav_item_class'] . '--' . $args['current_page_class'] . ' ' . $args['current_page_class']; }
			echo '">';
			echo '<a href="' . untrailingslashit(get_pagenum_link($max_num_pages)) . '" class="' . $args['nav_item_class'] . '__link">' . $args['last_page_text'] . '</a></li>';
		}

		echo '</ul>';

	}
}


/*
 * Page navigation
 */

function dazy_page_navi() {
	
	global $wp_query;
	$bignum = 999999999;
	if ( $wp_query->max_num_pages <= 1 )
	return;

    $links = paginate_links( array(
		'base' => str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
		'format' => '',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages,
		'end_size' => 3,
		'mid_size' => 2,
		'prev_next' => false,
		'type' => 'array'
    ) );

    if ( $links ) :

	    echo '<ul class="pagination">';

	    // get_previous_posts_link will return a string or void if no link is set.
	    if ( $prev_posts_link = get_previous_posts_link( __( 'Föregående', 'dazy' ) ) ) :
	        echo '<li class="pagination__item pagination__item--prev">';
	        echo $prev_posts_link;
	        echo '</li>';
	    endif;

	    echo '<li class="pagination__item">';
	    echo join( '</li><li class="pagination__item">', $links );
	    echo '</li>';

	    // get_next_posts_link will return a string or void if no link is set.
	    if ( $next_posts_link = get_next_posts_link( __( 'Nästa', 'dazy' ) ) ) :
	        echo '<li class="pagination__item pagination__item--next">';
	        echo $next_posts_link;
	        echo '</li>';
	    endif;
	    echo '</ul>';

	endif;

} /* end page navi */
