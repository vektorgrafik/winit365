<?php
/*
|--------------------------------------------------------------------------
| ACF-templates
|--------------------------------------------------------------------------
|
| Here we can register default acf-layouts for flexible content.
| It's a breeze. Simply fill out the array below with the templates
| you want loaded per post-type.
|
*/
global $acf_templates;

// Edit this to your content.
/*$acf_templates = [
	'page' => [
		[
			'title' => 'Min första fina template',
			'layouts' => [
				'2col_text_image',
				'2col_text_image',
				'2col_text_image'
			]
		],
		[
			'title' => 'Min andra fina template',
			'layouts' => [
				'2col_text_quotes',
				'2col_lists'
			]
		],
	]
];*/

function vektor_has_acf_templates() {
	global $acf_templates;
	return is_array( $acf_templates );
}

function vektor_get_acf_templates( $post_type ) {
	global $acf_templates;
	return ( array_key_exists( $post_type, $acf_templates )) ? $acf_templates[$post_type] : false;
}

function vektor_attach_acf_template_scripts( $hook ) {

	global $post_type;
	$pages = ['post-new.php', 'post.php' ];

	if( !in_array( $hook, $pages ))
		return false;

	if( !vektor_has_acf_templates() )
		return false;

	if( !vektor_get_acf_templates( $post_type ))
		return false;

	wp_enqueue_script( 'vektor-acf-templates', get_template_directory_uri() . '/assets/js/admin/vektor-acf-templates.js' );
	wp_localize_script( 'vektor-acf-templates', 'vektor_acf_templates', vektor_get_acf_templates( $post_type ));

}
add_action( 'admin_enqueue_scripts', 'vektor_attach_acf_template_scripts' );