<?php

function dazy_post_types() {


	register_post_type( 'people',
		array('labels' => array(
			'name' => __('Medarbetare', 'dazy'),
			'singular_name' => __('Medarbetare', 'dazy'),
			'all_items' => __('Alla Medarbetare', 'dazy'),
			'add_new' => __('Lägg till ny', 'dazy'),
			'add_new_item' => __('Lägg till ny medarbetare', 'dazy'),
			'edit' => __( 'Redigera', 'dazy' ),
			'edit_item' => __('Redigera medarbetare', 'dazy'),
			'new_item' => __('Ny medarbetare', 'dazy'),
			'view_item' => __('Visa medarbetare', 'dazy'),
			'search_items' => __('Sök bland medarbetare', 'dazy'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-groups',
			'rewrite'	=> array( 'slug' => __('medarbetare', 'vektor'), 'with_front' => false ),
			'has_archive' => 'cpt',
			'capability_type' => 'post',
			'hierarchical' => false,

			'supports' => array( 'title', 'thumbnail' )
	 	)
	);

	register_post_type( 'calendar',
		array('labels' => array(
			'name' => __('Kalender', 'dazy'),
			'singular_name' => __('Kalender', 'dazy'),
			'all_items' => __('Alla Kalenderhändelser', 'dazy'),
			'add_new' => __('Lägg till ny', 'dazy'),
			'add_new_item' => __('Lägg till ny kalenderhändelse', 'dazy'),
			'edit' => __( 'Redigera', 'dazy' ),
			'edit_item' => __('Redigera kalenderhändelse', 'dazy'),
			'new_item' => __('Ny kalenderhändelse', 'dazy'),
			'view_item' => __('Visa kalenderhändelse', 'dazy'),
			'search_items' => __('Sök bland kalenderhändelser', 'dazy'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-admin-post',
			'rewrite'	=> array( 'slug' => __('events', 'vektor'), 'with_front' => false ),
			'has_archive' => 'cpt',
			'capability_type' => 'post',
			'hierarchical' => false,

			'supports' => array( 'title' )
	 	)
	);

	register_post_type( 'pressrelease',
		array('labels' => array(
			'name' => __('Pressmeddelanden', 'dazy'),
			'singular_name' => __('Pressmeddelande', 'dazy'),
			'all_items' => __('Alla pressmeddelanden', 'dazy'),
			'add_new' => __('Lägg till ny', 'dazy'),
			'add_new_item' => __('Lägg till nytt pressmeddelande', 'dazy'),
			'edit' => __( 'Redigera', 'dazy' ),
			'edit_item' => __('Redigera pressmeddelande', 'dazy'),
			'new_item' => __('Nytt pressmeddelande', 'dazy'),
			'view_item' => __('Visa pressmeddelande', 'dazy'),
			'search_items' => __('Sök bland pressmeddelanden', 'dazy'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-admin-post',
			'rewrite'	=> array( 'slug' => __('pressreleases', 'vektor'), 'with_front' => false ),
			'has_archive' => 'cpt',
			'capability_type' => 'post',
			'hierarchical' => false,

			'supports' => array( 'title', 'editor' )
	 	)
	);

	register_post_type( 'reports',
		array('labels' => array(
			'name' => __('Rapporter', 'dazy'),
			'singular_name' => __('Rapport', 'dazy'),
			'all_items' => __('Alla rapporter', 'dazy'),
			'add_new' => __('Lägg till ny', 'dazy'),
			'add_new_item' => __('Lägg till ny rapporter', 'dazy'),
			'edit' => __( 'Redigera', 'dazy' ),
			'edit_item' => __('Redigera rapporter', 'dazy'),
			'new_item' => __('Ny rapporter', 'dazy'),
			'view_item' => __('Visa rapporter', 'dazy'),
			'search_items' => __('Sök bland rapporter', 'dazy'),
			'not_found' =>  __('Nothing found in the Database.', 'vektor'),
			'not_found_in_trash' => __('Nothing found in Trash', 'vektor'),
			'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4,
			'menu_icon' => 'dashicons-admin-post',
			'rewrite'	=> array( 'slug' => __('reports', 'vektor'), 'with_front' => false ),
			'has_archive' => 'cpt',
			'capability_type' => 'post',
			'hierarchical' => false,

			'supports' => array( 'title', 'editor' )
	 	)
	);


	/* this adds your post categories to your custom post type */
	// register_taxonomy_for_object_type('category', 'custom_type');

	/* this adds your post tags to your custom post type */
	// register_taxonomy_for_object_type('post_tag', 'custom_type');

}
add_action( 'init', 'dazy_post_types');

// Template for Custom Taxonomies
register_taxonomy( 'people_tax',
	array('people'),
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Kategorier', 'vektor' ),
			'singular_name' => __( 'Kategori', 'vektor' ),
			'search_items' =>  __( 'Sök bland kategorier', 'vektor' ),
			'all_items' => __( 'Alla kategoirer', 'vektor' ),
			'parent_item' => __( 'Föräldrakategori', 'vektor' ),
			'parent_item_colon' => __( 'Föräldrakategori:', 'vektor' ),
			'edit_item' => __( 'Redigera kategori', 'vektor' ),
			'update_item' => __( 'Uppdatera kategori', 'vektor' ),
			'add_new_item' => __( 'Lägg till ny kategori', 'vektor' ),
			'new_item_name' => __( 'Ny kategori namn', 'vektor' )
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'medarbetare-kategori' ),
	)
);

register_taxonomy( 'pressrelease_tax',
	array('pressrelease'),
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Kategorier', 'vektor' ),
			'singular_name' => __( 'Kategori', 'vektor' ),
			'search_items' =>  __( 'Sök bland kategorier', 'vektor' ),
			'all_items' => __( 'Alla kategoirer', 'vektor' ),
			'parent_item' => __( 'Föräldrakategori', 'vektor' ),
			'parent_item_colon' => __( 'Föräldrakategori:', 'vektor' ),
			'edit_item' => __( 'Redigera kategori', 'vektor' ),
			'update_item' => __( 'Uppdatera kategori', 'vektor' ),
			'add_new_item' => __( 'Lägg till ny kategori', 'vektor' ),
			'new_item_name' => __( 'Ny kategori namn', 'vektor' )
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'pressmeddelande-kategori' ),
	)
);

register_taxonomy( 'reports_tax',
	array('reports'),
	array('hierarchical' => true,
		'labels' => array(
			'name' => __( 'Kategorier', 'vektor' ),
			'singular_name' => __( 'Kategori', 'vektor' ),
			'search_items' =>  __( 'Sök bland kategorier', 'vektor' ),
			'all_items' => __( 'Alla kategoirer', 'vektor' ),
			'parent_item' => __( 'Föräldrakategori', 'vektor' ),
			'parent_item_colon' => __( 'Föräldrakategori:', 'vektor' ),
			'edit_item' => __( 'Redigera kategori', 'vektor' ),
			'update_item' => __( 'Uppdatera kategori', 'vektor' ),
			'add_new_item' => __( 'Lägg till ny kategori', 'vektor' ),
			'new_item_name' => __( 'Ny kategori namn', 'vektor' )
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array( 'slug' => 'rapporter-kategori' ),
	)
);
