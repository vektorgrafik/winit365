<?php

class Cision_Import {

//    public static $cision_rss_release_url = 'http://publish.ne.cision.com/Release/GetReleaseDetail?releaseId=';
    public static $cision_rss_url = 'http://publish.ne.cision.com/papi/NewsFeed/65A073C6562A45F481D5AAD6F3876A54?format=json';

    public static $cision_detailed_json_start = 'http://publish.ne.cision.com/papi/Release/';
    public static $cision_detailed_json_end = '?format=json';

//    public static $cision_rss_url = 'http://publish.ne.cision.com/Release/GetDetail/1912FDF1E2A4B4E5';


    public static function get_cision_posts(){

        $content = file_get_contents( self::$cision_rss_url );
//        $x = new SimpleXmlElement($content);
        $x = json_decode($content);
        $posts = array();

        foreach($x->Releases as $entry) {
            $posts[] = [
                'title' => 	(string) $entry->Title,
                'guid' => 	(string) $entry->EncryptedId,
                'date' =>	(string) date('Y-m-d H:i:s', strtotime($entry->PublishDate)),
            ];
        }

        return $posts;

    }

    /**
     * Returns detailed data about cision release
     * @param  string $encr_id The ID to get data from
     * @return array $post_data Array with detailed post data
     */
    public static function get_cision_post_data( $encr_id = null ){

        // Required to specify $guid
        if(!$encr_id) return;

        $content = file_get_contents( self::$cision_detailed_json_start . $encr_id . self::$cision_detailed_json_end);
        $x =  json_decode($content);


        $post_data = [
            'intro' => 		(string) preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $x->Release->HtmlTitle),
            'content' => 	preg_replace('/(<[^>]+) style=".*?"/i', '$1', $x->Release->HtmlBody)
        ];

        return $post_data;

    }

    public static function sync_cision_posts(){
        // Get Cision posts with data
        $posts = self::get_cision_posts();

        // Require posts or return
        if( empty($posts) ) return;

        $count = 0;
        foreach( $posts as $post ):
            $count++;
        if($count < 5){

            $post_data = self::get_cision_post_data($post['guid'] );
            $new_post = array(
                'post_type' => 		'pressrelease',
                'post_date' => 		$post['date'],
                'post_title' => 	$post['title'],
                'post_status' => 	'publish',
                'post_content' => 	$post_data['content'],
//                  TODO fixa kategorier ( press, rapport, regulations)
//                'post_category' =>  [4]
            );

            if( self::cision_post_exists($post['guid']) ){

                $new_post['ID'] = self::cision_post_exists($post['guid']);
            }

            $post_id = wp_insert_post($new_post);
            if(isset($post_id)):

                update_field('intro', $post_data['intro'], $post_id);
                update_field('cision_guid', $post['guid'], $post_id);
                error_log('inlagd');
            endif;

        }

        endforeach;

    }

    /**
     * Check if there's an existing post with the given guid and return true or false
     * @param  String $guid The unique identifier for a press release
     * @return Boolean      [description]
     */
    public static function cision_post_exists( $guid = null ){

        // Required to specify $guid
        if(!$guid) return;

        $existing_press_release = new WP_Query(array(
            'post_type' => 'pressrelease',
            'meta_query' => array(
                array(
                    'key' => 'cision_guid',
                    'value' => $guid,
                    'compare' => '=',
                )
            )
        ));

        return $existing_press_release->have_posts() ? $existing_press_release->posts[0]->ID : false;

    }

}

$cision_import = new Cision_Import;
