<?php
// Adds your styles to the WordPress editor
add_action( 'init', 'add_editor_styles' );
function add_editor_styles() {
    add_editor_style( get_template_directory_uri() . '/assets/css/editor.css' );
}

// Adds your styles to the WordPress editor
add_action( 'admin_enqueue_scripts', 'add_admin_styles' );
function add_admin_styles() {
  	wp_enqueue_style('admin-styles', get_template_directory_uri() . '/assets/css/admin.css');
  	//wp_enqueue_style('script-font', 'https://fonts.googleapis.com/css?family=Rock+Salt');
}
